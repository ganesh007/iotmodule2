# IIOT PROTOCOLS

## 1) 4-20mA Current Loop

* A 4-20mA is more widely used protocol in the industries.
* It is used for carring data from the sensor and then received at the other end through a receiver and it processed according.

### How it works 

1) It works on the same principle as the ohm's law.  

   OHM's LAW states that " V = I x R"  

2) We can see a circuit given below we can have 'n' number of resistors not just one.

3) So we power the circuit with a voltage supply and the current flows in the circuit will be the same as oer the ohm's law.

4) There a voltage drop across each of these resisitors so the value of voltage keeps changing in betweent the circuit.

5) Since the value of current remains same it is chosen as the parameter to measure the sensor's data.

![jfd](https://myelectrical.com/Portals/0/SunBlogNuke/2/Windows-Live-Writer/Back-to-Basics---Ohms-Law_B80A/0243_OhmsLawR_2.png)

**CURRENT AND WATER FLOW ANALOGY**

![photo](https://science4fun.info/wp-content/uploads/2017/05/voltage-current-resistance-analogy.jpg)

* The similarities in betweent them are;
 
    * Voltage ~ Pressure   

    * Loads/Resistance ~ Flow restrictions

    * Current ~ Flow 

* So we can use this current circuit analogy to any circuit which has similarity in the parameters.

### 4-20mA CIRCUIT

![photo](https://instrumentationforum.com/uploads/db4532/original/2X/5/5efe89813fdedacff698009f6e8d6ea4a264ffc0.png)

1) **SENSOR** 

    * So what happens here is the sensor detects a change in humidity,flow,pressure etc.   

2) **TRANSMITTER** 

    * Whenever the sensor is monitoring, the transmitter converts this data/info into an electrical signal. 

    * Like lets say the sensor was measuring a tank if its full or not. If it full it will send a current value of **20mA** further into the circuit.
  **4mA** if it empty, if it is half full **12mA** is sent as the signal.

3) **POWER SOURCE** 

    * So as seen in the current circuit before we have a similar circuit here.

    * We have power source just as the water system we have a water pressure.

    * We have to keep in mind that it needs to be **DC** voltage supply beacuse we are expecting a constant current value. If we chose a AC voltage supply the current value constantly changes.

4) **LOOP** 

    * So we see that there loop, one part connects the transmitter to the reciever and the other is receiver back to the transmitter forming a loop.

    * While the wire it self has resistance it is over looked if it is small circuit but not if it bigger than 1000 feet long and also the thickness of the wire.

5) **RECEIVER**

    * At the end of one part of the loop there is a receiver, receiving data from the sensor.

    * The current value received by the receiver is the converted into a data which makes sense like the feet of liquid in the tank.

    * This part might also contain a display screen to show the information.


So these are the components required to complete a 4-20mA current loop.

### Advantages and Disadvantages 

**Advantages** 

* The 4-20 mA current loop is the dominant standard in many industries.

* It is the simplest option to connect and configure.

* Since 4 mA is equal to 0% output, it is incredibly simple to detect a fault in the system.

**DIsdvantages**

* Current loops can only transmit one particular process signal.

* Multiple loops must be created in situations where there are numerous process variables that require transmission. Running so much wire could lead to problems with ground loops if independent loops are not properly isolated.


## 2) MODBUS 

* There can be used over **ETHERNET** as well as **SERIAL CABLE**

* The three major variations of MODBUS Protocols are:
    * ASCII
    * RTU
    * TCP/IP 


Modbus RTU devices typically use any one if the three electrical intefaces,

* **RS 232** 

     * It is a simple arrangement.

     * If we only need to connect one device to another which is less than 50ft in distance between them, then RS 232 can be used.

* **RS 485 & RS 422** 
    * If we need to connect connect more devices on the same line which are at a distance greater than 50ft then these 2 can be used.


#### RS 485

* It is the most popular method.

* It follows a **Master** and **Slave** method.

* It can have only one master and and the rest as slaves(max. 247 Devices).

* Each of these slaves have a unique ID from 1 to 247.

* RS 485 can not connect more than 32 nodes(devices) in a single segment, but by using a repeater more than 32 devices can be connected.

* It can support a serial/daisy chain fashion and not star/parallel fashion.

![hd](https://gitlab.com/ganesh007/iotmodule2/-/raw/master/assignments/summary/Extras/modbus.png)


## 3) OPC UA 

* It is short for **Open Platform Communications United Architecture**.

* OPCUA is an open source industrial standard.

* Used as a data exchange standard for industrial communication (machine to machine OR PC to machine).

* It consists of a security for encryption of data exchanged. 

* The architecture of a OPC application is independent of whether it is the server or client part, is structured into levels.

![js](https://www.opc-router.de/wp-content/uploads/2019/08/OPC-UA-over-TSN_600x400px_en.png)



# CLOUD PROTOCOLS 


## 1) MQTT 

* Short for **Message Query Telemetry Transport** 

* Used as a messaging protocol for devices with low bandwidth.

* Communication between several devices can be established simultaneously.

![ib](https://gitlab.com/iotiotdotin/project-internship/iotmodule2/-/wikis/uploads/358848b4b9222917e31db143ff0a6383/Main-entities-of-the-Message-Queuing-Telemetry-Transport-MQTT-protocol.png)

#### WORKING 

* So it has a **MQTT client** , **MQTT broker**.

* MQTT client can be a **PUBLISHER** or a **SUBSCRIBER**

* So lets say a **MQTT client(Publisher)** publishes a **messages** on temperature infomation.

* These **messsages** go to the **MQTT Broker** as **Topics**.

* These Topics can be **subscribed** by the other clients for receiving updates on that topics.

* These topics are represented by strings, separated by forward slash. Each slash indicates a topic level.

![bbi](https://www.opc-router.de/wp-content/uploads/2020/01/MQTT_Schema_EN.jpg)



## 2) HTTP 

* Short for **"Hyper Text Transfer Protcol"**.

* Used for transfering messages over the internet.

* HTTP - Without encryption
  
  HTTPS - With encrytion (S stands for Secure)

* Uses request-response method to communicate between client and server. 

* Think of it as a phone call, so you are calling the person ( here it is a _**website url**_) , the call is recevied by the tower(_**Server**_) , the tower connects you to the person you want to talk to (_**website page**_ you requested for).

![jbo](https://gitlab.com/iotiotdotin/project-internship/iotmodule2/-/wikis/uploads/85f443d40e36e1f5bcb4e71ac14205b0/http-req-res.png)

* A HTTP request consists of

  * The request line - The server could be asked to send the resource to the client.

  * HTTP headers- These are written on a message to provide the recipient with information about the message, the sender, and the way in which the sender wants to communicate with the recipient.

  * Body - Content of any HTTP message can be referred to as a message body.


* A HTTP response consists of 

    * Status line
    * HTTP header
    * Message body

* Each protocol uses a particular **PORT** as a medium for communication so as to control internet traffic.

* HTTP uses port **80**

  HTTPS uses port **443**

The server will send you send you different status codes depending on situation

![hv](https://gitlab.com/iotiotdotin/project-internship/iotmodule2/-/wikis/uploads/1dcbcc2c7e58e59a042e55b2b2080405/restful-web-services-with-spring-mvc-28-638.jpg)
