# 1.1 Sensors and Actuators

![photo](https://gitlab.com/iotiotdotin/project-internship/iotmodule2/-/wikis/uploads/0*NIzIJMOgy6Aich2m.png)

## Sensors 

* They are basically a type of transducer meaning they convert one form of energy into an other.  
* But sensors are those transducers which convert a physical energy to an electrical signal.   
* Here are some the sensors that are used in IoT.  

![photo](https://gitlab.com/ganesh007/iotmodule2/-/raw/master/assignments/summary/Extras/Common-sensor-categories-including-magnetic-sensors-in-IoT-applications-Magnetic-sensors__1_.png)

## Actuators 

* Actutators are the opposite of sensors.  
* An actuator is a device that makes something move or operate. It receives a source of energy and uses it to move something.  

Here are some of the types os actuators use in industries.

![photo](https://realpars.com/wp-content/uploads/2020/02/Actuators-Type-1280x720.png)  


# 1.2 Analog and Digital

These are the types of signals which is used to transmit data.  

### Analog Vs Digital 

![differemce between a and d](https://qph.fs.quoracdn.net/main-qimg-13a98ddcd24ca901c619212bc24e24f0)

![photo](https://cdn1.byjus.com/wp-content/uploads/2018/11/physics/2016/11/25112340/Analog-and-Digital.png)  

# 1.3 Micro-controllers and Micro-Processors  

* **Microcontrollers** - They are integrated circuits designed to govern a specific operation in an embedded system.  

    * ``` They do not need an OS, interpreter, Firmware. ```

    * ``` They have an internal memory. ( Ex: RAM , ROM) ```

    * ``` Ex: Arduino. ```

* **MicroProcessors** - It is an electronic component that is used by a computer to do its work.   

    * ``` They need an OS. ```  

    * ``` They do not have internal memory. ```

    * ``` Ex: Raspberry Pi. ```

### Microcontroller Vs microprocessors 

![Vs](https://eeeproject.com/wp-content/uploads/2017/10/Microprocessor-vs-Microcontroller.jpg)  


# 1.4 Introduction to RPi

![kn](https://www.piday.org/wp-content/uploads/2018/09/word-image-2.png)


![hfs](https://3.bp.blogspot.com/-AaF-1bgwKPU/VtiILibFsAI/AAAAAAAAA3E/CCiGrn0Ui8I/s1600/Raspberry%2BPi%2B3%2B-%2BModel%2BB%2Btech.png)

### Interfaces 

Interfaces are the different ways in which you can connect different software/hardware components and make them exchange information between them. 

Some of the different interfaces used by RPi are,

  * GPIO
  * UART
  * SPI
  * I2C
  * PWM

![hdi](https://i.imgur.com/S3PVovs.jpg)

## Serial Vs Parallel 

#### Serial Interfaces

A serial interface is a communication interface that transmits data as a single stream of bits, typically using a wire-plus-ground cable, a single wireless channel or a wire-pair.  
Examples: 
 * RS-232
 * I2C
 * UART

#### Parallel Interfaces

A parallel interface refers to a multiline channel, with each line capable of transmitting several bits of data simultaneously.
Examples: 
* GPOI

![ol](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a6/Parallel_and_Serial_Transmission.gif/300px-Parallel_and_Serial_Transmission.gif)